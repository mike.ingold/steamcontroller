#define EVENT_SIZE sizeof(struct input_event)

/*
 *   getinputevent(*fp)
 * Retrieve an evdev input_event from a given event handle. This function
 * operates on a pointer instead of returning an object in order to permit a
 * null response.
 *
 * Arguments:
 *   struct input_event *answer - pointer used to return the received event
 *   FILE * fp - pointer to an active event file (e.g. /dev/input/eventXX)
 */
void getinputevent(struct input_event *answer, FILE *fp)
{
	// Validate that the given handle is non-null
	if ( fp == NULL ) {
		answer = NULL;
		return;
	}

	// Perform a blocking read into a buffer, re-cast it, and return
	struct input_event *buffer = (struct input_event *)malloc(EVENT_SIZE);
	fread(buffer, EVENT_SIZE, 1, fp);
	*answer = *buffer;
}

void update_evabs(struct SteamController *controller, struct input_event *event)
{
	switch (event->code) {
	case SC_AXIS_ANALOG_X:
		controller->axes.analog_x = event->value;
		break;
	case SC_AXIS_ANALOG_Y:
		controller->axes.analog_y = event->value;
		break;
	case SC_AXIS_LTOUCH_X:
		controller->axes.ltouch_x = event->value;
		break;
	case SC_AXIS_LTOUCH_Y:
		controller->axes.ltouch_y = event->value;
		break;
	case SC_AXIS_RTOUCH_X:
		controller->axes.rtouch_x = event->value;
		break;
	case SC_AXIS_RTOUCH_Y:
		controller->axes.rtouch_y = event->value;
		break;
	case SC_AXIS_RTRIGGER:
		controller->axes.rtrigger = event->value;
		break;
	case SC_AXIS_LTRIGGER:
		controller->axes.ltrigger = event->value;
		break;
	default:
		// TODO log?
		break;
	}
}

void update_evkey(struct SteamController *controller, struct input_event *event)
{
	switch (event->code) {
	case SC_BUTTON_LTOUCH_TOUCHING:
		controller->buttons.ltouch_touching = (char)event->value;
		break;
	case SC_BUTTON_RTOUCH_TOUCHING:
		controller->buttons.rtouch_touching = (char)event->value;
		break;
	case SC_BUTTON_A:
		controller->buttons.a = (char)event->value;
		break;
	case SC_BUTTON_B:
		controller->buttons.b = (char)event->value;
		break;
	case SC_BUTTON_X:
		controller->buttons.x = (char)event->value;
		break;
	case SC_BUTTON_Y:
		controller->buttons.y = (char)event->value;
		break;
	case SC_BUTTON_LBUMPER:
		controller->buttons.lshoulder = (char)event->value;
		break;
	case SC_BUTTON_RBUMPER:
		controller->buttons.rshoulder = (char)event->value;
		break;
	case SC_BUTTON_LTRIGGER_CLICK:
		controller->buttons.ltrigger_click = (char)event->value;
		break;
	case SC_BUTTON_RTRIGGER_CLICK:
		controller->buttons.rtrigger_click = (char)event->value;
		break;
	case SC_BUTTON_SELECT:
		controller->buttons.select = (char)event->value;
		break;
	case SC_BUTTON_START:
		controller->buttons.start = (char)event->value;
		break;
	case SC_BUTTON_STEAM:
		controller->buttons.steam = (char)event->value;
		break;
	case SC_BUTTON_RTOUCH_CLICK:
		controller->buttons.rtouch_click = (char)event->value;
		break;
	case SC_BUTTON_LGRIP:
		controller->buttons.lgrip = (char)event->value;
		break;
	case SC_BUTTON_RGRIP:
		controller->buttons.rgrip = (char)event->value;
		break;
	case SC_BUTTON_DPAD_U:
		controller->buttons.dpad_u = (char)event->value;
		break;
	case SC_BUTTON_DPAD_D:
		controller->buttons.dpad_d = (char)event->value;
		break;
	case SC_BUTTON_DPAD_L:
		controller->buttons.dpad_l = (char)event->value;
		break;
	case SC_BUTTON_DPAD_R:
		controller->buttons.dpad_r = (char)event->value;
		break;
	default:
		// TODO log?
		break;
	}	
}

void update(struct SteamController *controller, struct input_event *event)
{
	if ( event->type == EV_KEY ) {
		update_evkey(controller, event);
	} else if ( event->type == EV_ABS ) {
		update_evabs(controller, event);
	}
}
