// NOTE: controller has Y axes inverted by default

#define SC_AXIS_ANALOG_X  0x0000
#define SC_AXIS_ANALOG_Y  0x0001
#define SC_AXIS_LTOUCH_X  0x0010
#define SC_AXIS_LTOUCH_Y  0x0011
#define SC_AXIS_RTOUCH_X  0x0003
#define SC_AXIS_RTOUCH_Y  0x0004
#define SC_AXIS_RTRIGGER  0x0014
#define SC_AXIS_LTRIGGER  0x0015

#define SC_BUTTON_LTOUCH_TOUCHING 0x0121
#define SC_BUTTON_RTOUCH_TOUCHING 0x0122
#define SC_BUTTON_A               0x0130
#define SC_BUTTON_B               0x0131
#define SC_BUTTON_X               0x0133
#define SC_BUTTON_Y               0x0134
#define SC_BUTTON_LBUMPER         0x0136
#define SC_BUTTON_RBUMPER         0x0137
#define SC_BUTTON_LTRIGGER_CLICK  0x0138
#define SC_BUTTON_RTRIGGER_CLICK  0x0139
#define SC_BUTTON_SELECT          0x013a
#define SC_BUTTON_START           0x013b
#define SC_BUTTON_STEAM           0x013c
#define SC_BUTTON_RTOUCH_CLICK    0x013e
#define SC_BUTTON_LGRIP           0x0150
#define SC_BUTTON_RGRIP           0x0151
#define SC_BUTTON_DPAD_U          0x0220
#define SC_BUTTON_DPAD_D          0x0221
#define SC_BUTTON_DPAD_L          0x0222
#define SC_BUTTON_DPAD_R          0x0223

// Struct to store state of all analog inputs on a Steam Controller
struct SteamController_Axes {
	__s32 analog_x;
	__s32 analog_y;
	__s32 ltouch_x;
	__s32 ltouch_y;
	__s32 rtouch_x;
	__s32 rtouch_y;
	__s32 ltrigger;
	__s32 rtrigger;
};

// Struct to store state of all binary inputs on a Steam Controller
struct SteamController_Buttons {
	char a;
	char b;
	char x;
	char y;
	char select;
	char start;
	char steam;
	char ltrigger_click;
	char rtrigger_click;
	char ltouch_touching;
	char dpad_l;
	char dpad_r;
	char dpad_u;
	char dpad_d;
	char rtouch_touching;
	char rtouch_click;
	char lgrip;
	char rgrip;
	char lshoulder;
	char rshoulder;
};

// Struct to store state of all inputs on a Steam Controller
struct SteamController {
	struct SteamController_Axes axes;
	struct SteamController_Buttons buttons;
};

/*    SteamController()
 *  Construct and return a SteamController struct with default null values.
 */
struct SteamController SteamController()
{
	struct SteamController controller;

	// Initialize axes to zero
	controller.axes.analog_x = 0;
	controller.axes.analog_y = 0;
	controller.axes.ltouch_x = 0;
	controller.axes.ltouch_y = 0;
	controller.axes.rtouch_x = 0;
	controller.axes.rtouch_y = 0;
	controller.axes.ltrigger = 0;
	controller.axes.rtrigger = 0;

	// Initialize buttons to zero
	controller.buttons.a = 0;
	controller.buttons.b = 0;
	controller.buttons.x = 0;
	controller.buttons.y = 0;
	controller.buttons.select = 0;
	controller.buttons.start = 0;
	controller.buttons.steam = 0;
	controller.buttons.ltrigger_click = 0;
	controller.buttons.rtrigger_click = 0;
	controller.buttons.ltouch_touching = 0;
	controller.buttons.dpad_l = 0;
	controller.buttons.dpad_r = 0;
	controller.buttons.dpad_u = 0;
	controller.buttons.dpad_d = 0;
	controller.buttons.rtouch_touching = 0;
	controller.buttons.rtouch_click = 0;
	controller.buttons.lgrip = 0;
	controller.buttons.rgrip = 0;
	controller.buttons.lshoulder = 0;
	controller.buttons.rshoulder = 0;

	// Copy given fname and return
	return controller;
};

