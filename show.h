#define MODE_DISPLAY_QUIET   0
#define MODE_DISPLAY_RAW     1
#define MODE_DISPLAY_VERBOSE 2

#define LENGTH_STRING_TIME 24         // YYYYMMDD-HH:MM:SS.SSSSSS
#define LENGTH_STRING_DESCRIPTION 40

char * show_evtype(__u16 type)
{
	// defined in /usr/include/linux/input-event-codes.h
	switch (type) {
	case EV_SYN:
		return "EV_SYN";
	case EV_KEY:
	        return "EV_KEY";
	case EV_REL:
	        return "EV_REL";
	case EV_ABS:
	        return "EV_ABS";
	case EV_MSC:
	        return "EV_MSC";
	case EV_SW:
	        return "EV_SW";
	case EV_LED:
	        return "EV_LED";
	case EV_SND:
	        return "EV_SND";
	case EV_REP:
	        return "EV_REP";
	case EV_FF:
	        return "EV_FF";
	case EV_PWR:
	        return "EV_PWR";
	case EV_FF_STATUS:
	        return "EV_FF_STATUS";
	case EV_MAX:
	        return "EV_MAX";
	case EV_CNT:
	        return "EV_CNT";
	default:
		return "UNKOWN";
	}
}

char * show_evcode_button(__u16 code)
{
	switch (code) {
	case SC_AXIS_ANALOG_X:
		return "Analog_X";
	case SC_AXIS_ANALOG_Y:
		return "Analog_Y";
	case SC_AXIS_LTOUCH_X:
		return "LTouch_X";
	case SC_AXIS_LTOUCH_Y:
		return "LTouch_Y";
	case SC_AXIS_RTRIGGER:
		return "RTrigger";
	case SC_AXIS_RTOUCH_X:
		return "RTouch_X";
	case SC_AXIS_RTOUCH_Y:
		return "RTouch_Y";
	case SC_AXIS_LTRIGGER:
		return "LTrigger";
	case SC_BUTTON_LTOUCH_TOUCHING:
		return "LTouch_Touching";
	case SC_BUTTON_RTOUCH_TOUCHING:
		return "RTouch_Touching";
	case SC_BUTTON_A:
		return "A";
	case SC_BUTTON_B:
		return "B";
	case SC_BUTTON_X:
		return "X";
	case SC_BUTTON_Y:
		return "Y";
	case SC_BUTTON_LBUMPER:
		return "LBumper";
	case SC_BUTTON_RBUMPER:
		return "RBumper";
	case SC_BUTTON_LTRIGGER_CLICK:
		return "LTrigger_Click";
	case SC_BUTTON_RTRIGGER_CLICK:
		return "RTrigger_Click";
	case SC_BUTTON_SELECT:
		return "Select";
	case SC_BUTTON_START:
		return "Start";
	case SC_BUTTON_STEAM:
		return "SteamButton";
	case SC_BUTTON_RTOUCH_CLICK:
		return "RTouch_Click";
	case SC_BUTTON_LGRIP:
		return "LGrip";
	case SC_BUTTON_RGRIP:
		return "RGrip";
	case SC_BUTTON_DPAD_U:
		return "LTouch_DPad_U";
	case SC_BUTTON_DPAD_D:
		return "LTouch_DPad_D";
	case SC_BUTTON_DPAD_L:
		return "LTouch_DPad_L";
	case SC_BUTTON_DPAD_R:
		return "LTouch_DPad_R";
	default:
		return "UNKNOWN";
	}
}

void show_event_time(char * answer, struct input_event *event)
{
	struct tm *ltime;
	char tm_str[LENGTH_STRING_TIME];
	char usec_str[8];

	// Derive localtime from seconds since UNIX epoch, generate string
	ltime = localtime(&event->time.tv_sec);
	strftime(tm_str, sizeof(tm_str), "%Y%m%d-%H:%M:%S", ltime);

	// Generate string from usec's, append
	sprintf(usec_str, ".%06ld", event->time.tv_usec);
	strcat(tm_str, usec_str);

	// Copy derived string to provided pointer
	strcpy(answer, tm_str);
}

void show_event_description(char * answer, struct input_event *event, char mode)
{
	// If raw mode is selected then jump straight to fallback condition
	if ( mode == MODE_DISPLAY_RAW ) goto SHOW_RAW;

	// Describe the input_event based on it's type
	switch ( event->type ) {
	case EV_SYN:
		sprintf(answer, "Synchronization Marker");
		break;
	case EV_KEY:
		sprintf(answer, "Key %s = %s",
			show_evcode_button(event->code),
			( event->value ? "ON" : "OFF" )  );
		break;
	case EV_REL:
		sprintf(answer, "Axis %s + %d",
			show_evcode_button(event->code),
			event->value  );
		break;
	case EV_ABS:
		sprintf(answer, "Axis %s = %d",
			show_evcode_button(event->code),
			event->value  );
		break;
	default:
		SHOW_RAW:
		sprintf(answer, "type %d, code %d, value %d",
			event->type,
			event->code,
			event->value  );
		break;
	}
}

/*
 *   display(*event)
 * Pretty-print data from an input_event.
 * Arguments:
 *   struct input_event *event - the subject to be printed
 */
void display(struct input_event *event, char mode)
{
	// No action if given null pointer or null input_event
	if ( ( event == NULL ) || ( event->time.tv_sec == 0 ) ) return;

	char str_time[LENGTH_STRING_TIME];
	char str_description[LENGTH_STRING_DESCRIPTION];

	// If display mode is _QUIET or not defined/supported
	if ( ( mode != MODE_DISPLAY_RAW ) && ( mode != MODE_DISPLAY_VERBOSE ) )
		return;

	// Generate string components and print
	show_event_time(str_time, event);
	show_event_description(str_description, event, mode);
	printf("%s> %s\n", str_time, str_description);
}

