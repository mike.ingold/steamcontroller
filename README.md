# SteamController C interface for Linux

This is a simple C library for interfacing with a Steam Controller in Linux.

## Compatibility

This library leverages the Steam Controller driver interface released with Linux kernel 4.18 (12 August 2018).

Support for Raspberry Pi Zero W has been verified. As of late 2020, even the RaspberryPi OS distros compiled for 32-bit/ARMv6 variants should have recent enough kernels by default.
