#include <stdio.h>
#include <stdlib.h>
#include <linux/input.h>
#include <time.h>
#include <string.h>

// SteamController state structs, constructor
#include "struct.h"

// Linux evdev API event management
#include "events.h"

// Display-related functions
#include "show.h"

